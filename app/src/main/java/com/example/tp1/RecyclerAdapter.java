package com.example.tp1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp1.data.Country;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView CountryFlag;
        TextView countryName;
        TextView countryCapital;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            CountryFlag = itemView.findViewById(R.id.flag);
            countryName = itemView.findViewById(R.id.country);
            countryCapital = itemView.findViewById(R.id.capital);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();

                    // pas reussi à envoyer la position au second fragment
                    NavDirections direction = ListFragmentDirections.actionListFragmentToDetailFragment();
                    Navigation.findNavController(v).navigate(direction);
                }
            });
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        String uri = Country.countries[i].getImgUri();
        Context c = viewHolder.CountryFlag.getContext();
        viewHolder.CountryFlag.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri , null , c.getPackageName())));
        viewHolder.countryName.setText(Country.countries[i].getName());
        viewHolder.countryCapital.setText(Country.countries[i].getCapital());
    }

    @Override
    public int getItemCount() {
        return Country.countries.length;
    }
}
